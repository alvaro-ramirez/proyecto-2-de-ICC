package mx.unam.ciencias.icc.proyecto2;

import mx.unam.ciencias.icc.Lista;
import mx.unam.ciencias.icc.proyecto2.Banderas;
import mx.unam.ciencias.icc.proyecto2.Lectura;
import mx.unam.ciencias.icc.proyecto2.Escritura;
import mx.unam.ciencias.icc.proyecto2.OrdenadorLexicografico;

/**
 * <p> Clase principal del proyecto 2: Ordenador Lexicografico</p>
 * @autor Alvaro Ramirez Lopez
 */
public class Proyecto2 {
    
    public static void main(String[] args) {
	
	Banderas banderasArgs = new Banderas(args); 
	Lista<String> listaPrincipal; 
	Lectura lector = new Lectura(args); 
	
	listaPrincipal = lector.comoLeer(banderasArgs.esBanderaO() != -1 || banderasArgs.esBanderaR()); 
	OrdenadorLexicografico ordenador = new OrdenadorLexicografico(listaPrincipal);
	listaPrincipal = ordenador.ordenador(); 
	if (banderasArgs.esBanderaR())
	    listaPrincipal = listaPrincipal.reversa();
	
	if (banderasArgs.esBanderaO() != -1){
	    banderasArgs.activaBanderaO(listaPrincipal);
	    for (String imprime : listaPrincipal)
		System.out.println(imprime); 
	} else {
		for (String imprime : listaPrincipal)
		System.out.println(imprime);
	}
  }
}