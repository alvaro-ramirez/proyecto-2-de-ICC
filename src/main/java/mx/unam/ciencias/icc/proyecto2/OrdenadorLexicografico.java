package mx.unam.ciencias.icc.proyecto2;

import java.text.Collator;
import mx.unam.ciencias.icc.Lista;

/**
 * <p> Clase OrdenadorLexicografico </p>
 * <p> Esta clase nos ayudara a comparar los parrafos 
 * que ya esten en una lista y que se comparen importando
 * una clase del paquete Java.text</p>
 */

public class OrdenadorLexicografico{

  	/* Variable de clase */
    private Lista<String> lista;

    /**
     * Constructor de la clase
     * @param lista la lista que ordenaremos.
     */
    public OrdenadorLexicografico(Lista<String> lista) {
	   this.lista = lista;
    }

  	/**
     * Método ordenador: ordenara la lista usando un Collator.
     * @return listaOrdenada la cadena de la lista ya ordenada.
     */
    public Lista<String> ordenador() {
	Lista<String> listaOrdenada;
	Collator collator = Collator.getInstance();
	collator.setStrength(Collator.TERTIARY);
	listaOrdenada = lista.mergeSort((str1, str2) -> collator.compare
				   (str1.replaceAll("\\P{L}+", ""), str2.replaceAll("\\P{L}+", "")));
	return listaOrdenada;
    }
}