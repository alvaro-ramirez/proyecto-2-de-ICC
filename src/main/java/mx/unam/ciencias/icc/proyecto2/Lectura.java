package mx.unam.ciencias.icc.proyecto2;

import mx.unam.ciencias.icc.Lista;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.IOException;


/**
 * <p> Clase Lectura: Esta clase nos ayudara a leer multiples 
 * archivos que se pasen por el los args del main o por la 
 * entrada estandar de la consola</p>
 */

public class Lectura {

     /* Variable que recibira args del main del proyecto2.*/
    private String[] args;

    /**
     * El contructor recibira los args del main.
     * @param args los args del main, donde se contiene los archivos.
     */
    public Lectura(String[] args){
	this.args = args;
    }
  
    /**
     * Metodo comoLeer: Evalua los args del main para saber 
     * cual metodo invocar, si <code>leerEntradaEstandar</code> 
     * o si <code>leerArchivos</code>
     */
    public Lista<String> comoLeer(boolean banderasArgs) {
	return (esEntrada(banderasArgs)) ? leerEntradaEstandar() : leerArchivos(banderasArgs);
    }
    
    /**
     * Método esEntrada: para determinar si es entrada estandar o no.
     * @param banderasArgs boolean que indica si tiene banderas.
     * @return true si esta o false si no.
     */
    public boolean esEntrada(boolean banderasArgs) {
	return (args.length == 0 || args.length == 1 && banderasArgs) ? true : false;
    }

    /**
     * Método leerEntradaEstandar: para leer el archivo en entrada estandar.
     * @return lista la lista que tendrá las cadenas del archivo.
     */
    private Lista<String> leerEntradaEstandar() {
	Lista<String> lista = new Lista<>();
	try (BufferedReader br = new BufferedReader(new InputStreamReader((System.in),"UTF-8"))) {
	    String lineaActual = br.readLine();
	    while (lineaActual != null) {
		lista.agregaFinal(lineaActual);
		lineaActual = br.readLine();
	     }
	  }
	catch (IOException e) {
	    System.out.println("Error: hubo un problema en la entrada estandar");
	    System.exit(1);
	  }
	return lista;
    }

    /** 
     * Método leerArchivos: para leer un archivo o multiples archivos.
     * @return lista la lista que tendrá las cadenas del archivo.
     */
    private Lista<String> leerArchivos(boolean banderasArgs) {
	Lista<String> lista = new Lista<>();
	
	for (int i = 0; i < args.length; i++) {
	    if (!((args[i].contains("-r") || args[i].contains("-o")) && args[i].length() <= 3)) {
		try (BufferedReader br = new BufferedReader
		     (new InputStreamReader(new FileInputStream(args[i]),"UTF-8"))) {		
		    String lineaActual;
		    while ((lineaActual = br.readLine()) != null)
			lista.agregaFinal(lineaActual);
		}
		catch (IOException e) {
		    if (args[i -1].contains("-o") && args[i-1].length() <= 3)
			continue;
		    else {
			System.out.println("Fallo al leer el archivo");
			System.exit(1);
		     }
		   }
	    }
	}
	return lista;
  }
}