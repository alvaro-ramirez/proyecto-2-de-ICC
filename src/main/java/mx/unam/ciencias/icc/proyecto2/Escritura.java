package mx.unam.ciencias.icc.proyecto2;

import mx.unam.ciencias.icc.Lista;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;

/**
 * <p> Clase Escritura </p>
 *
 * <p> Esta clase nos ayudara a escribir una lista a un archivo con un identificador ya establecido</p>
 *
 */

public class Escritura {

    /* Variable de clase*/
    private Lista<String> lista;

    /**
     * El contructor recibira una lista.
     * @param lista la lista que se le pasara para que se escriba en el archivo.
     */
    public Escritura(Lista<String> lista) {
		this.lista = lista;
    }
    
    /**
     * Método escribeEn: Escribe la lista que lo invoque con el identificador ya establecido.
     * @param escribe cadena con el nombre/ruta del archivo donde escribiremos.
     */    
    public void escribeEn(String identificador) {
	try (Writer escribe = new BufferedWriter
	     (new OutputStreamWriter(new FileOutputStream(identificador), "UTF-8"))) {
	    escribe.write(lista.eliminaPrimero());
	    while (!lista.esVacia()) {
		escribe.write("\n" + lista.eliminaPrimero());
	    }
	}
	catch (IOException e) {
	    System.out.println("Hubo un error al escribir");
	  }       
    }
}