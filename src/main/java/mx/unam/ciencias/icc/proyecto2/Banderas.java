package mx.unam.ciencias.icc.proyecto2;

import mx.unam.ciencias.icc.Lista;
import mx.unam.ciencias.icc.proyecto2.Escritura;

/**
 * <p>Clase las dos banderas que manejara el programa </p>
 *
 * <p>Las dos banderas que manejara el programa seran -r y -o</p>
 *
 * <p>La clase hace uso de dos clases para el comportamiento, de la clase Lista
 * y de la clase Escritura </p>
 *
 */
public class Banderas {
    
    /* Variable para recibir los argumentos del main*/
    private String[] args;
    
    /**
     * El constructor de la clase Banderas
     * @param args los argumentos del main. 
     */
    public Banderas(String[] args) {
	   this.args = args;
    }
    
    /**
     * Método activaBanderaO: Este metodo checara la posision siguiente 
     * de la bandera -o no es nula creara un archivo y escribira en el, 
     * esta accion es autodestructiva.
     * @param lista una lista de donde sacara el contenido del archivo.
     */
    public void activaBanderaO(Lista<String> lista) {
	try {
	    if (args[esBanderaO() +1] != null);
	}
	catch (ArrayIndexOutOfBoundsException e) {
	    System.err.println("La bandera -o debe estar seguida de un identificador");
	    System.exit(1);
	}
	
    Escritura escritor = new Escritura(lista);
	escritor.escribeEn(args[esBanderaO() +1]);
    }
    
    /**
     * Método esBanderaO: Revisa si hay una bandera -o y si se encontro 
     * se regresara el indice de su ubicacion, la posicion siguiente de 
     * la bandera sera el identificador para escribir
     * @return <code>i</code> en el caso que encontrara una bandera i y 
     * <code>-1</code> en el caso que no encontrara
     */
    public int esBanderaO() {
	for (int i = 0; i < args.length; i++) {
	    if (args[i].contains("-") && args[i].contains("o") && args[i].length() <= 3)
		return i;
	  }
	return -1;
    }

    /**
     * Método esBanderaR: Revisa si hay una bandera -r:
     * @return <code>true</code>, en el caso que si la encuentre 
     * @return <code>false</code>, en el caso que no la encuentre.
     */
    public boolean esBanderaR() {
	for (int i = 0; i < args.length; i++) {
	    if (args[i].contains("-") && args[i].contains("r") && args[i].length() <= 3)
		return true;
	  }
	return false;
    }
}
