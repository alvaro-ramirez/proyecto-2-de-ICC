Introducción a Ciencias de la Computación
=========================================

Proyecto 2: Ordenador lexicográfico
-------------------------------------

### Fecha de entrega: Viernes 26 de abril, 2019

Deben implementar un ordenador lexicográfico (una versión más sencilla del programa sort en Unix) que funcione con uno o más archivos de texto o la entrada estándar y que imprima su salida en la salida estándar.

Por ejemplo, si tienen el achivo hombres.txt:

```shell
Hombres necios que acusáis
    a la mujer sin razón,
sin ver que sois la ocasión
    de lo mismo que culpáis.

Si con ansia sin igual
    solicitáis su desdén,
¿por qué queréis que obren bien
    si las incitáis al mal?

Combatís su resistencia
    y luego con gravedad
decís que fue liviandad
    lo que hizo la diligencia.

Parecer quiere el denuedo
    de vuestro parecer loco
al niño que pone el coco
    y luego le tiene miedo.

Queréis con presunción necia
    hallar a la que buscáis,
para pretendida, Tais,
    y en la posesión, Lucrecia.

¿Qué humor puede ser más raro
    que el que, falto de consejo,
él mismo empaña el espejo
    y siente que no esté claro?
```
Entonces al correr su proyecto de la siguiente manera:

```shell
$ java -jar proyecto2.jar hombre.txt
```

La salida estándar del programa debe ser:

```shell




    a la mujer sin razón,
al niño que pone el coco
Combatís su resistencia
decís que fue liviandad
    de lo mismo que culpáis.
    de vuestro parecer loco
él mismo empaña el espejo
    hallar a la que buscáis,
Hombres necios que acusáis
    lo que hizo la diligencia.
para pretendida, Tais,
Parecer quiere el denuedo
¿por qué queréis que obren bien
    que el que, falto de consejo,
¿Qué humor puede ser más raro
Queréis con presunción necia
Si con ansia sin igual
    si las incitáis al mal?
sin ver que sois la ocasión
    solicitáis su desdén,
    y en la posesión, Lucrecia.
    y luego con gravedad
    y luego le tiene miedo.
    y siente que no esté claro?
```